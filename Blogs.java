package blog;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Blogs{
    public String readBlogInfo() {
        Scanner scanner= new Scanner(System.in);
        System.out.println("Enter blog name: ");
        String blogName = scanner.next();
        System.out.println("\nEnter The Range : ");
        int range[] = new int[2];
        for (int i = 0; i <range.length; i++) {
        range[i] = scanner.nextInt();
        }
        scanner.close();
        String url ="https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + range[1] + "&start="+ range[0];
        return url;
    }
    public String fetchBlogInfo(String urlInString) {
        try { 
            String responseRead = "";
            String responseString = ""; 
            URL url=new URL(urlInString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.connect();
            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
            responseRead += scanner.nextLine();
            }
            scanner.close();
            while (responseRead .startsWith("var tumblr_api_read = ")) {
            responseString = responseRead .substring("var tumblr_api_read = ".length());
            responseString= responseString.replace(";", "");
            break;
            }
            return responseString;
        } catch (IOException exception) {
            exception.printStackTrace();
            return "";
        } 
    }

    public void displayBlogInfo(String response) {
        try {
            JSONObject BlogObject = new JSONObject(response);
            JSONObject blogKeyObject = new JSONObject(BlogObject.getJSONObject("tumblelog").toString());
            System.out.println("Title : " + blogKeyObject.get("title").toString());
            System.out.println("Description : " + blogKeyObject.get("description").toString());
            System.out.println("Name : " + blogKeyObject.get("name").toString());
            System.out.println("Number Of Posts : " + BlogObject.get("posts-total").toString());

            JSONArray postsArray = new JSONArray(BlogObject.getJSONArray("posts").toString());
            for (int index = 0; index < postsArray.length(); index++) {
                JSONObject postsObject = new JSONObject(postsArray.get(index).toString());
                System.out.println((index + 1) + ":" + postsObject.get("photo-url-1280").toString());
            }
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        } 
    }
    
    public void initiater(){
        Blogs blog = new Blogs();
	String url=readBlogInfo();
        String blogResponse = blog.fetchBlogInfo(url);
        blog.displayBlogInfo(blogResponse);
    }


    public static void main(String[] args) {
       initiater();
    }
}
