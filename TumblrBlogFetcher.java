import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TumblrBlogFetcher{

public static void main(String[] args) {
TumblrBlogFetcher blog = new TumblrBlogFetcher();
String urlInString = blog.readBlogInfo();
String blogResponse = blog.fetchBlogInfo(urlInString);
String blogJSON = blog.removeVarName(blogResponse);
blog.displayBlogInfo(blogJSON);
}

public String readBlogInfo() {
Scanner scanner= new Scanner(System.in);
String blogName, url;
int range[] = new int[2];
System.out.println("Enter blog name: ");
blogName = scanner.next();
System.out.println("\nEnter The Range : ");
for (int i = 0; i <range.length; i++) {
range[i] = scanner.nextInt();
}
scanner.close();
url = "https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + range[1] 
+ "&start=" + range[0];
return url;
}

public String removeVarName(String responseRead ) {
String responseString = "";
String variableName = "var tumblr_api_read = ";
while (responseRead .startsWith(variableName)) {
responseString = responseRead .substring(variableName.length());
responseString = responseString.replace(";", "");
break;
}
return responseString;
}

public String fetchBlogInfo(String urlInString) {
try { 
String responseRead = "";
URL url = new URL(urlInString);
Scanner scanner = new Scanner(url.openStream());
HttpURLConnection connection = (HttpURLConnection) url.openConnection();
connection.setRequestMethod("POST");
connection.connect();
while (scanner.hasNext()) {
responseRead += scanner.nextLine();
}
scanner.close();
return responseRead;
} 
catch (IOException exception) {
exception.printStackTrace();
return "";
} 
}

public void displayBlogInfo(String response) {
try {
JSONObject blogObject, blogKeyObject, postsObject;
JSONArray postsArray;
blogObject = new JSONObject(response);
blogKeyObject = new JSONObject(blogObject.getJSONObject("tumblelog").toString());
System.out.println("Title : " + blogKeyObject.get("title").toString());
System.out.println("Description : " + blogKeyObject.get("description").toString());
System.out.println("Name : " + blogKeyObject.get("name").toString());
System.out.println("Number Of Posts : " + blogObject.get("posts-total").toString());
postsArray = new JSONArray(blogObject.getJSONArray("posts").toString());
for (int index = 0; index < postsArray.length(); index++) {
postsObject = new JSONObject(postsArray.get(index).toString());
System.out.println((index + 1) + ":" + postsObject.get("photo-url-1280").toString());
}
} 
catch (JSONException jsonException) {
jsonException.printStackTrace();
} 
}
}
